/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kulisara.finalproject;

/**
 *
 * @author Cyberpunk 2077
 */
public class Food {
    private String Menu;
    private int Amount;
    private String Description;

    @Override
    public String toString() {
        return "Food{" + "Menu=" + Menu + ", Amount=" + Amount + ", Description=" + Description + '}';
    }

    public String getMenu() {
        return Menu;
    }

    public void setMenu(String Menu) {
        this.Menu = Menu;
    }

    public int getAmount() {
        return Amount;
    }

    public void setAmount(int Amount) {
        this.Amount = Amount;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public Food(String Menu, int Amount, String Description) {
        this.Menu = Menu;
        this.Amount = Amount;
        this.Description = Description;
    }

}
