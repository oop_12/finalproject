/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kulisara.finalproject;

/**
 *
 * @author Cyberpunk 2077
 */
public class Drink {
    private String Water;
    private int Amount1;

    @Override
    public String toString() {
        return "Drink{" + "Water=" + Water + ", Amount1=" + Amount1 + '}';
    }

    public String getWater() {
        return Water;
    }

    public void setWater(String Water) {
        this.Water = Water;
    }

    public int getAmount1() {
        return Amount1;
    }

    public void setAmount1(int Amount1) {
        this.Amount1 = Amount1;
    }

    public Drink(String Water, int Amount1) {
        this.Water = Water;
        this.Amount1 = Amount1;
    }
    
}
